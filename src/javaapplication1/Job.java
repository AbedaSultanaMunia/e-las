package javaapplication1;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author abeda
 */
public class Job {

    HashMap<String, String> map
            = new HashMap<>();

    Random rand = new Random();
    private int number_of_gpu, epochs, finished_epoch, turn;
    float pr, service_time;
    private String ID;
    private long resume_time, duration;
    int finished_time, start_time;
    boolean is_finished = false;
    String timelas;
    String timeelas;
    String timeYARN;
    String timeSRTF;
    int queuing_delay_las = 0;
    int queuing_delay_elas = 0;
    int queuing_delay_YARN = 0;
    int queuing_delay_SRTF = 0;
    int last_Service_Unit = 0;
    boolean is_active = false;
    int eprate;
    HashMap<String, PrintWriter> file_writer
            = new HashMap<>();
    PrintWriter fw;
    File f;
    BufferedReader br;

    Job(PrintWriter writer, int i, int ep, int number_of_gpu) throws IOException {
        this.ID ="Data/"+ i + "ep_norm.txt";
        f = new File(ID);
        br = new BufferedReader(new FileReader(f));
        this.number_of_gpu = number_of_gpu;
        this.epochs = ep;

        this.pr = 0;
        this.turn = 0;

        //  fw = new PrintWriter(new FileWriter(this.ID + ".txt", true));
        //  fw.println("Job ID:" + this.ID + " #GPU:" + this.number_of_gpu + " epochs:" + this.epochs);
    }

   

    /* Job(PrintWriter writer, int i, int number_of_gpu, int toteps, int eprate) throws IOException {
         this.ID = i + "ep_pois.txt"; 
        f = new File(ID);
        br = new BufferedReader(new FileReader(f));
        this.number_of_gpu = number_of_gpu;
        this.epochs = toteps;//Math.abs(rand.nextInt()) % 3000 + 500;
       //+ "_" + String.valueOf(System.currentTimeMillis());
        this.pr = 0;
        this.turn = 0;
        this.finished_epoch = 0;
        this.eprate = eprate;

        int queuing_delay = 0;
        this.last_Service_Unit = 0;

    }*/
    void reinitialization() throws IOException {

        f = new File(ID);
        br = new BufferedReader(new FileReader(f));
        service_time = 0;
        finished_time = 0;
        start_time = 0;
        finished_epoch = 0;
        is_active = false;
        resume_time = 0;
        turn = 0;
        last_Service_Unit = 0;
        is_finished = false;

    }

    void reinitialization2() throws IOException {

        f = new File(ID);
        br = new BufferedReader(new FileReader(f));

        finished_time = 0;
        start_time = 0;
        finished_epoch = 0;
        is_active = false;
        last_Service_Unit = 0;
        resume_time = 0;
        turn = 0;
        is_active = false;
        is_finished = false;

    }

    double calc_pr() {

        if (turn == 0) {
            return 0;
        }
        double pr = (((double) service_time * number_of_gpu * epochs * (get_epochs() - get_finished_epoch()))
                / (get_finished_epoch() / turn));

        //System.out.println(pr+" "+ID);
        return pr;

    }

    double calc_st() {

        if (turn == 0) {
            return 0;
        }
        double pr = (double) service_time * (double) number_of_gpu;
        return pr;

    }

    void update_remaining_epochs(int x, float time) {
        finished_epoch += x;
        //service_time += time;

    }

    void update_service_time(int time) {
        service_time += time;
        this.turn++;
    }

    void update_service_time2(int time) {
        //service_time += time;
        this.turn++;
    }

    String get_ID() {
        return ID;
    }

    int get_gpu() {
        return number_of_gpu;
    }

    int get_turn() {
        return turn;
    }

    int get_finished_epoch() {
        return finished_epoch;
    }

    int get_epochrate() {
        return eprate;
    }

    int get_epochs() {
        return epochs;
    }

    void set_start_time(int turn, PrintWriter logwriter) {
        start_time = turn;
        this.turn = start_time;
        //logwriter.println(get_ID() + "#Start Time:#" + start_time + "\n");
    }

    int get_start_time() {
        return start_time;
    }

    void set_resume_time() {
        resume_time = System.currentTimeMillis();
    }

    void set_finished_time(int turn, PrintWriter logwriter) {
        finished_time = turn;
        logwriter.println(get_ID() + "Start time  " + start_time + " #Finish Time: " + finished_time + " GPU: " + number_of_gpu + " ep: " + epochs + "\n");
    }

    int get_finished_time() {
        return finished_time;
    }

    void set_job_done() {
        is_finished = true;
    }

    void set_job_active() {
        is_active = true;
    }

    void set_job_inactive(int turn) {
        last_Service_Unit = turn;
        is_active = false;
    }

    void add_queueingdelay_las(int current_turn) {
        queuing_delay_las += (current_turn - last_Service_Unit);
    }

    void add_queueingdelay_elas(int current_turn) {
        //  System.out.println(ID+": "+current_turn+" - "+last_Service_Unit);
        queuing_delay_elas += (current_turn - last_Service_Unit);
    }

    void add_queueingdelay_YARN(int current_turn) {
        //System.out.println(ID+": "+current_turn+" - "+last_Service_Unit);
        queuing_delay_YARN += (current_turn - 0);
    }

    void add_queueingdelay_SRTF(int current_turn) {
        queuing_delay_SRTF += (current_turn - 0);
    }

    void job_done() {
        //  finished_time = System.currentTimeMillis();

        String str = String.valueOf(start_time) + " " + String.valueOf(finished_time);
        map.put(ID, str);
    }

}
