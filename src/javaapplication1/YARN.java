/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication1;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author abeda
 */
public class YARN {

    List<Job> Job_list = new ArrayList<>();
    List<Job> active_job_list = new ArrayList<>();

    int Total_GPU, remaining_gpu, Turn = 1;
    Thread thread = new Thread();
    PrintWriter logWriter;
    int Distribution;

    YARN(List JobList, int gpus, PrintWriter log_writer, int dist) throws IOException, InterruptedException {
        System.setProperty("java.util.Arrays.useLegacyMergeSort", "true");
        Job_list = JobList;
        Total_GPU = gpus;
        remaining_gpu = Total_GPU;
        logWriter = log_writer;
        Distribution = dist;
        schedule();
    }

    void schedule() throws IOException, InterruptedException {
        int sum = 0, sum2 = 0;
        for (int i = 0; i < 20; i++) {
            sum += Job_list.get(i).service_time + sum2;
            sum2 = sum;
        }
        boolean flag = true;
        while (Job_list.size() != 0) {
            List<Job> list = new ArrayList<>();
            int remaining_GPU;
            // synchronized (this) {
            remaining_GPU = get_remaining_gpu();
            // }
            int prev_turn = Turn;

            for (int i = 0; i < Job_list.size() && remaining_GPU > 0; i++) {
                Job j = Job_list.get(i);
                if (j.is_active == true) {
                    continue;
                }
                if (j.is_finished) {
                    j.br.close();
                    j.timeYARN = j.get_ID() + " Finished at " + j.get_finished_time() + " Service time: " + j.service_time;
                    list.add(j);
                } else if (remaining_GPU - j.get_gpu() >= 0) {

                    if (Turn == prev_turn) {
                        Turn++;
                    }
                    set_remaining_gpu(j.get_gpu());
                    remaining_GPU -= j.get_gpu();
                    j.set_job_active();
                    j.set_start_time(Turn, logWriter);
                    //  System.out.println("Turn: " + Turn + " Job: " + j.get_ID());
                    System.out.print("");
                    j.add_queueingdelay_YARN(Turn);
                    // schedule(j);
                    schedule(j);
                }
                // }
            }

            for (int j = 0; j < list.size(); j++) {
                // System.out.println(list.get(j).get_ID() + " finished at " + list.get(j).get_finished_time()+" Service time: "+list.get(j).service_time);

                Job_list.remove(list.get(j));

            }
        }
    }

    void schedule(Job j) throws IOException, InterruptedException {

        Job_Thread_YARN jt = new Job_Thread_YARN(j, Turn, this, logWriter, Distribution, active_job_list);
    }

    synchronized int get_remaining_gpu() {
        return remaining_gpu;
    }

    synchronized void set_remaining_gpu(int x) {
        remaining_gpu -= x;
    }

    synchronized void release_gpu(int x) {
        remaining_gpu += x;
    }

    void increase_TURN() {
        Turn++;
    }

    class Job_Thread_YARN implements Runnable {

        Job job;
        int service_quantum = 5, Turn;
        YARN sch;
        PrintWriter logwriter;
        int Distribution;
        List<Job> active_job_list;

        Job_Thread_YARN(Job J, int Turn, YARN sch, PrintWriter logWriter, int dist, List<Job> active_job_list) throws IOException, InterruptedException {

            job = J;
            this.Turn = Turn;
            this.sch = sch;
            logwriter = logWriter;
            Distribution = dist;
            this.active_job_list = active_job_list;
            //get_distribution_value(Distribution);
            Thread thread = new Thread(this);
            thread.start();

        }

        @Override
        public void run() {
            try {

                get_distribution_value(Distribution);
            } catch (IOException ex) {
                System.out.println(ex);
            } catch (InterruptedException ex) {
                Logger.getLogger(Job_Thread_YARN.class.getName()).log(Level.SEVERE, null, ex);
            }

        }

        void get_distribution_value(int dist) throws IOException, InterruptedException {
            PrintWriter fw = job.fw;
            boolean flag = true;
            int epoch_number = 0;
            int epoch_i = 0;
            String str;
            // service_quantum = (int)job.service_time;
            for (int i = 0;; i++) {
                if (i % service_quantum == 0) {
                    epoch_i = 0;
                    increase_TURN();
                }

                if ((str = job.br.readLine()) != null) {
                    job.update_remaining_epochs(Integer.parseInt(str), System.currentTimeMillis());
                } else {
                    job.set_finished_time(sch.Turn, logwriter);

                    job.set_job_done();
                    break;
                }

                if (job.get_finished_epoch() == 0) {

                    job.set_start_time(Turn, logwriter);
                }

            }

            job.set_job_done();
            job.set_finished_time(sch.Turn, logwriter);
            job.set_job_inactive(sch.Turn);

            sch.release_gpu(job.get_gpu());

        }

    }

}
