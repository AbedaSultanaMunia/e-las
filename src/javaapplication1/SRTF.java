/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication1;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author abeda
 */
public class SRTF {

    List<Job> Job_list = new ArrayList<>();
    List<Job> active_job_list = new ArrayList<>();

    int Total_GPU, remaining_gpu, Turn = 1;
    Thread thread = new Thread();
    PrintWriter logWriter;
    int Distribution;

    public SRTF(List JobList, int gpus, PrintWriter log_writer, int dist) throws IOException, InterruptedException {
        System.setProperty("java.util.Arrays.useLegacyMergeSort", "true");
        Job_list = JobList;
        Total_GPU = gpus;
        remaining_gpu = Total_GPU;
        logWriter = log_writer;
        Distribution = dist;
        Job_sort();

        schedule();
    }

    public SRTF(int gpu) {
        remaining_gpu += gpu;
    }

    void schedule() throws IOException, InterruptedException {

        boolean flag = true;
        while (Job_list.size() != 0) {
            List<Job> list = new ArrayList<>();

            int remaining_GPU;
            //synchronized (this) {
            remaining_GPU = get_remaining_gpu();
            //   }`
            int prev_turn = Turn;

            for (int i = 0; i < Job_list.size() && remaining_GPU > 0; i++) {
                Job j = Job_list.get(i);
                if (j.is_active) {
                    continue;
                }
                // synchronized (this) {
                if (j.is_finished) {
                    j.br.close();
                    j.timeSRTF = j.get_ID() + " finished at " + j.get_finished_time() + " Service time: " + j.service_time;
                    list.add(j);

                } else if (remaining_GPU - j.get_gpu() >= 0) {
                    if (Turn == prev_turn) {
                        Turn++;
                    }
                    set_remaining_gpu(j.get_gpu());
                    remaining_GPU -= j.get_gpu();
                    j.set_job_active();
                    j.set_start_time(Turn, logWriter);
                    System.out.print("");
                    j.add_queueingdelay_SRTF(Turn);
                    //schedule(j);
                   schedule(j);
                }
                // }
            }
            for (int j = 0; j < list.size(); j++) {
                Job_list.remove(list.get(j));
            }
        }

    }
    void schedule(Job j) {

        Job_Thread jt = new Job_Thread(j, Turn, this, logWriter, Distribution, active_job_list);
    }

    

    int getPoissonRandom() {
        double mean = 30;
        Random r = new Random();
        double L = Math.exp(-mean);
        int k = 0;
        double p = 1.0;
        do {
            p = p * r.nextDouble();
            k++;
        } while (p > L);
        return k - 1;
    }

    int getNormalRandom(Job job) {
        int epoch_i;
        do {
            double val = job.rand.nextGaussian() * 2 + 30;

            epoch_i = (int) Math.round(val);

        } while (epoch_i <= 0);
        return epoch_i;
    }

    int getExpoDist(Job j) {
        Random r = j.rand;

        double p = 1 / 70.0;
        int y = (int) -(Math.log(r.nextDouble()) / p);
        return y;
    }

    int getConstantDist(Job j) {
        return 10;
    }

    int getincreasingDist(Job j, int epoch_i) {
        return epoch_i + 50;
    }

    int getdecreasingDist(Job j, int epoch_i) {
        return epoch_i - 50;
    }

    synchronized int get_remaining_gpu() {
        return remaining_gpu;
    }

    synchronized void set_remaining_gpu(int x) {
        remaining_gpu -= x;
    }

    synchronized void release_gpu(int x) {
        remaining_gpu += x;
    }

    void Job_sort() {
        // System.out.println(Job_list.size());
       // for(int i=0 ; i< 20; i++)
         //   System.out.println(Job_list.get(i).service_time);
        Collections.sort(Job_list, new SortbyServiceTime2());
    }

    void increase_TURN() {
        Turn++;
    }

    class Job_Thread implements Runnable {

        Job job;
        int service_quantum = 5, Turn;
        SRTF sch;
        PrintWriter logwriter;
        int Distribution;
        List<Job> active_job_list;

        Job_Thread(Job J, int Turn, SRTF sch, PrintWriter logWriter, int dist, List<Job> active_job_list) {

            job = J;
            this.Turn = Turn;
            this.sch = sch;
            logwriter = logWriter;
            Distribution = dist;
            this.active_job_list = active_job_list;
            Thread thread = new Thread(this);
            thread.start();

        }

        synchronized void release_gpu(int x) {
            remaining_gpu += x;
        }

        void increase_TURN() {
            Turn++;
        }

        @Override
        public void run() {
            try {

                get_distribution_value(Distribution);
            } catch (IOException ex) {
                System.out.println(ex);
            } catch (InterruptedException ex) {
                Logger.getLogger(Job_Thread.class.getName()).log(Level.SEVERE, null, ex);
            }

        }

        void get_distribution_value(int dist) throws IOException, InterruptedException {

            int service_quantum = 5;
            PrintWriter fw = job.fw;
            boolean flag = true;
            int epoch_number = 0;
            int epoch_i = 0;
            // service_quantum = (int)job.service_time;
            for (int i = 0;; i++) {

                if (i % service_quantum == 0) {
                    epoch_i = 0;
                    increase_TURN();
                }

                String str;
                if ((str = job.br.readLine()) != null) {
                    job.update_remaining_epochs(Integer.parseInt(str), System.currentTimeMillis());
                } else {
                    job.set_finished_time(Turn, logWriter);

                    job.set_job_done();
                    break;
                }

                if (job.get_finished_epoch() == 0) {

                    job.set_start_time(Turn, logWriter);
                }

            }
            job.set_job_done();
            job.set_finished_time(Turn, logWriter);
            job.set_job_inactive(Turn);
            // job.update_service_time(service_quantum);
            release_gpu(job.get_gpu());
            //   }

        }
    }

    class SortbyServiceTime2 implements Comparator<Job> {

        // Used for sorting in ascending order of 
        // roll number 
        @Override
        public int compare(Job a, Job b) {
            if (a.service_time < b.service_time) {
                return -1;
            } else if (a.service_time > b.service_time) {
                return 1;
            } else {
                return 0;
            }
        }
    }

}
