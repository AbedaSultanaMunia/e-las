/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication1;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author abeda
 */
public class E_LAS_utilization implements Runnable {

    List<Job> Job_list = new ArrayList<>();
    List<Job> active_job_list = new ArrayList<>();

    int Total_GPU, remaining_gpu, Turn = 1;
    Thread thread = new Thread();
    PrintWriter logWriter;
    int Distribution;
    int jobcounter = 0;
    float utilization = 0, utilization_counter = 0;

    E_LAS_utilization(List JobList, int gpus, PrintWriter log_writer, int dist) throws IOException {

        System.setProperty("java.util.Arrays.useLegacyMergeSort", "true");
        Job_list = JobList;
        Total_GPU = gpus;
        remaining_gpu = 60;
        logWriter = log_writer;
        Distribution = dist;
        schedule();
        //Thread thread = new Thread(this);
        //this.run();
    }

    void schedule() throws IOException {

        boolean flag = true;
        while (jobcounter != 480) {
            List<Job> list = new ArrayList<>();
            int remaining_GPU;
            // synchronized (this) {
            remaining_GPU = get_remaining_gpu();
            // }
            int prev_turn = Turn;

            Job_sort();

            int i = 0;
            int _flag = 0;
            for (; i < GpuUtilization.Job_list1.size() && remaining_GPU > 0; i++) {
                Job j = GpuUtilization.Job_list1.get(i);

                if (j.is_active == true) {
                    continue;
                }

                if (j.is_finished) {
                    // j.set_finished_time(Turn, logWriter);
                    j.timeelas = j.get_ID() + " Finished at " + j.get_finished_time() + " Service time: " + j.service_time;
                    j.br.close();
                    jobcounter++;
                    list.add(j);
                } else if (remaining_GPU - j.get_gpu() >= 0) {
                    if (Turn == prev_turn) {
                        Turn++;
                    }
                    set_remaining_gpu(j.get_gpu());
                    remaining_GPU -= j.get_gpu();
                    j.set_job_active();

                    if (j.get_finished_epoch() == 0) {

                        j.set_start_time(Turn, logWriter);
                    }

                    // System.out.println("Turn: " + Turn + " Job: " + j.get_ID());
                    //System.out.print("");
                    j.add_queueingdelay_elas(Turn);
                    schedule(j);
                    _flag++;

                } else {
                    //_flag++;

                    //break;
                }

                // System.out.println(" Turn: " + Turn + " Remaining Gpus: " + get_remaining_gpu());
            }
            if (_flag != 0) {
                
                    utilization += (60 - remaining_GPU) / 60.0;
                   // utilization_counter++;
                    System.out.println(" Utilization " + utilization + " Utilization counter: " + utilization_counter);
              
            }

            for (int j = 0; j < list.size(); j++) {
                synchronized (GpuUtilization.Job_list1) {
                    GpuUtilization.Job_list1.remove(list.get(j));
                }

            }
        }

        System.out.println("Scheduling done. Utilization: " + (utilization / Turn));
    }

    int get_job_counter() {
        return jobcounter;
    }

    void schedule(Job j) {

        Job_Thread jt = new Job_Thread(j, Turn, this, logWriter, Distribution, active_job_list);
    }

    synchronized int get_remaining_gpu() {
        return remaining_gpu;
    }

    synchronized void set_remaining_gpu(int x) {
        remaining_gpu -= x;
    }

    synchronized void release_gpu(int x) {
        remaining_gpu += x;
    }

    synchronized void Job_sort() {
        synchronized (GpuUtilization.Job_list1) {
            Collections.sort(GpuUtilization.Job_list1, new SortbyPr());
        }

    }

    @Override
    public void run() {

        //System.out.println("Scheduler");
        // schedule();
    }
}

class SortbyPr implements Comparator<Job> {

    @Override
    public int compare(Job a, Job b) {
        if (a.calc_pr() < b.calc_pr()) {
            return -1;
        } else if (a.calc_pr() > b.calc_pr()) {
            return 1;
        } else {
            return 0;
        }
    }
}

class Job_Thread implements Runnable {

    Job job;
    int service_quantum = 5, Turn;
    E_LAS_utilization sch;
    PrintWriter logwriter;
    int Distribution;
    List<Job> active_job_list;

    Job_Thread(Job J, int Turn, E_LAS_utilization sch, PrintWriter logWriter, int dist, List<Job> active_job_list) {

        job = J;
        this.Turn = Turn;
        this.sch = sch;
        logwriter = logWriter;
        Distribution = dist;
        this.active_job_list = active_job_list;
        Thread thread = new Thread(this);
        thread.start();

    }

    @Override
    public void run() {
        try {

            get_distribution_value(Distribution);
        } catch (IOException ex) {
            System.out.println(ex);
        } catch (InterruptedException ex) {
            Logger.getLogger(Job_Thread.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    void get_distribution_value(int dist) throws IOException, InterruptedException {
        PrintWriter fw = job.fw;
        boolean flag = true;
        int epoch_number = 0;
        int epoch_i = 0;
        for (int i = 0; i < service_quantum; i++) {

            String str;

            if ((str = job.br.readLine()) != null) {
                job.update_remaining_epochs(Integer.parseInt(str), System.currentTimeMillis());
                Thread.sleep(Integer.parseInt(str) * 10);
            } else {
                job.set_finished_time(sch.Turn, logwriter);

                job.set_job_done();
                break;
            }

        }

        job.set_job_inactive(Turn);
        job.update_service_time(service_quantum);
        sch.release_gpu(job.get_gpu());
        //   }

    }

    int getPoissonRandom() {
        double mean = 30;
        Random r = new Random();
        double L = Math.exp(-mean);
        int k = 0;
        double p = 1.0;
        do {
            p = p * r.nextDouble();
            k++;
        } while (p > L);
        return k - 1;
    }

    int getNormalRandom() {
        int epoch_i;
        do {
            double val = job.rand.nextGaussian() * 2 + 30;

            epoch_i = (int) Math.round(val);

        } while (epoch_i <= 0);
        return epoch_i;
    }

    int getExpoDist(Job j) {
        Random r = j.rand;

        double p = 1 / 70.0;
        int y = (int) -(Math.log(r.nextDouble()) / p);
        return y;
    }

    int getConstantDist(Job j) {
        return 10;
    }

    int getincreasingDist(Job j, int epoch_i) {
        return epoch_i + 50;
    }

    int getdecreasingDist(Job j, int epoch_i) {
        return epoch_i - 50;
    }
}
