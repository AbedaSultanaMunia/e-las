/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication1;

import java.awt.TexturePaint;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author abeda
 */
public class Scheduler {

    List<Job> Job_list = new ArrayList<>();
    List<Job> active_job_list = new ArrayList<>();

    int Total_GPU, remaining_gpu, Turn = 1;
    Thread thread = new Thread();
    PrintWriter logWriter;
    int Distribution;

    Scheduler(List JobList, int gpus, PrintWriter log_writer, int dist) throws IOException {
        System.setProperty("java.util.Arrays.useLegacyMergeSort", "true");
        Job_list = JobList;
        Total_GPU = gpus;
        remaining_gpu = Total_GPU;
        logWriter = log_writer;
        Distribution = dist;
        schedule();
    }

    public Scheduler(int gpu) {
        remaining_gpu += gpu;
    }

    void schedule() throws IOException {

        boolean flag = true;
        while (Job_list.size() != 0) {
            List<Job> list = new ArrayList<>();
            int remaining_GPU;
            // synchronized (this) {
            remaining_GPU = get_remaining_gpu();
            // }
            int prev_turn = Turn;
            System.out.println("br "+ Turn);
            Job_sort();
            for (int i = 0; i < Job_list.size() && remaining_GPU > 0; i++) {
                Job j = Job_list.get(i);
                if (j.is_active == true) {
                    continue;
                }

                if (j.is_finished) {
                    // j.set_finished_time(Turn, logWriter);
                    j.timeelas = j.get_ID() + " Finished at " + j.get_finished_time() + " Service time: " + j.service_time;
                    j.br.close();
                 
                    list.add(j);
                } else if (remaining_GPU - j.get_gpu() >= 0) {
                    if (Turn == prev_turn) {
                        Turn++;
                    }
                    set_remaining_gpu(j.get_gpu());
                    remaining_GPU -= j.get_gpu();
                    j.set_job_active();

                    if (j.get_finished_epoch() == 0) {

                        j.set_start_time(Turn, logWriter);
                    }

                    // System.out.println("Turn: " + Turn + " Job: " + j.get_ID());
                    System.out.print("");
                    j.add_queueingdelay_elas(Turn);
                    schedule(j);
                }
                // }
            }
              System.out.println("after "+Turn );

            for (int j = 0; j < list.size(); j++) {
                // System.out.println(list.get(j).get_ID() + " finished at " + list.get(j).get_finished_time()+" Service time: "+list.get(j).service_time);

                Job_list.remove(list.get(j));

            }
        }
    }

    void schedule(Job j) {

        Job_Thread jt = new Job_Thread(j, Turn, this, logWriter, Distribution, active_job_list);
    }

    synchronized int get_remaining_gpu() {
        return remaining_gpu;
    }

    synchronized void set_remaining_gpu(int x) {
        remaining_gpu -= x;
    }

    synchronized void release_gpu(int x) {
        remaining_gpu += x;
    }

    void Job_sort() {

        Collections.sort(Job_list, new SortbyPr());
    }
}

class SortbyPr implements Comparator<Job> {

    @Override
    public int compare(Job a, Job b) {
        if (a.calc_pr() < b.calc_pr()) {
            return -1;
        } else if (a.calc_pr() > b.calc_pr()) {
            return 1;
        } else {
            return 0;
        }
    }
}

class Job_Thread implements Runnable {

    Job job;
    int service_quantum = 5, Turn;
    Scheduler sch;
    PrintWriter logwriter;
    int Distribution;
    List<Job> active_job_list;

    Job_Thread(Job J, int Turn, Scheduler sch, PrintWriter logWriter, int dist, List<Job> active_job_list) {

        job = J;
        this.Turn = Turn;
        this.sch = sch;
        logwriter = logWriter;
        Distribution = dist;
        this.active_job_list = active_job_list;
        Thread thread = new Thread(this);
        thread.start();

    }

    @Override
    public void run() {
        try {

            get_distribution_value(Distribution);
        } catch (IOException ex) {
            System.out.println(ex);
        } catch (InterruptedException ex) {
            Logger.getLogger(Job_Thread.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    void get_distribution_value(int dist) throws IOException, InterruptedException {
        PrintWriter fw = job.fw;
        boolean flag = true;
        int epoch_number = 0;
        int epoch_i = 0;
        for (int i = 0; i < service_quantum; i++) {

            /*if (dist == 1) {
                epoch_i = getNormalRandom();
            } else if (dist == 2) {
                epoch_i = getPoissonRandom();
            } else if (dist == 3) {
                epoch_i = getConstantDist(job);
            } else if (dist == 4) {
                epoch_i = getincreasingDist(job, 100);
            } else if (dist == 5) {
                epoch_i = getdecreasingDist(job, 100);
            } else if (dist == 6) {
                epoch_i += job.get_epochrate();
            } else {
                epoch_i = getExpoDist(job);
            }*/
            String str;
          
            if ((str = job.br.readLine()) != null) {
                job.update_remaining_epochs(Integer.parseInt(str), System.currentTimeMillis());
            } else {
                job.set_finished_time(sch.Turn, logwriter);

                job.set_job_done();
                break;
            }

            /* if (job.get_epochs() >= job.get_finished_epoch()) {

            } else {
                job.set_finished_time(sch.Turn, logwriter);

                job.set_job_done();
                
                break;
            }
            job.update_remaining_epochs(epoch_i, System.currentTimeMillis());*/
        }

        job.set_job_inactive(Turn);
        job.update_service_time(service_quantum);
        sch.release_gpu(job.get_gpu());
        //   }

    }

    int getPoissonRandom() {
        double mean = 30;
        Random r = new Random();
        double L = Math.exp(-mean);
        int k = 0;
        double p = 1.0;
        do {
            p = p * r.nextDouble();
            k++;
        } while (p > L);
        return k - 1;
    }

    int getNormalRandom() {
        int epoch_i;
        do {
            double val = job.rand.nextGaussian() * 2 + 30;

            epoch_i = (int) Math.round(val);

        } while (epoch_i <= 0);
        return epoch_i;
    }

    int getExpoDist(Job j) {
        Random r = j.rand;

        double p = 1 / 70.0;
        int y = (int) -(Math.log(r.nextDouble()) / p);
        return y;
    }

    int getConstantDist(Job j) {
        return 10;
    }

    int getincreasingDist(Job j, int epoch_i) {
        return epoch_i + 50;
    }

    int getdecreasingDist(Job j, int epoch_i) {
        return epoch_i - 50;
    }
}
