/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication1;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javaapplication1.Job;
import javaapplication1.tiresias;

import org.apache.commons.math3.analysis.interpolation.InterpolatingMicrosphere;

/**
 *
 * @author abeda
 */
public class GpuUtilization {

    public static List JobQueue = new ArrayList<Job>();
    public static File file_arrival = new File("JobArrivalTime.txt");
    public static File file_totep = new File("normal.txt");
    public static File file_JobArrival_ID = new File("job_arrival_id.txt");
    static int total_gpu = 60;
    public static List<Job> Job_list1 = new ArrayList<>();
    public static List<Job> Job_list2 = new ArrayList<>();
    public static List<Job> Job_list_common = new ArrayList<>();
    public static PrintWriter writer;
    public static Thread th;

    public static void main(String[] args) throws FileNotFoundException, IOException, InterruptedException {
        BufferedReader br_arrival = new BufferedReader(new FileReader(file_arrival));
        BufferedReader br_totep = new BufferedReader(new FileReader(file_totep));
        BufferedReader br_job_arrival_id = new BufferedReader(new FileReader(file_JobArrival_ID));

        writer = new PrintWriter(new FileWriter("log_pois_expo.txt"));
        PrintWriter writer2 = new PrintWriter(new FileWriter("log_tri_pois_expo.txt"));
        PrintWriter writer4 = new PrintWriter(new FileWriter("log_YARN_pois_expo.txt"));
        PrintWriter writer5 = new PrintWriter(new FileWriter("log_SRTF_pois_n.txt"));
        
        //tiresias_scheduler();
        // PrintWriter writer3 = new PrintWriter(new FileWriter("JCT.txt", true));
        //ArrayList<String> Job_Desc = Job_Description();
        List<Integer> possible_gpu = gpu_descriptor();

        List<Integer> possible_job = job_descriptor();

       int Count = 0;

        //for(int p=10 ; p<=480 ; p+=10){
        File f = new File("normal.txt");
        PrintWriter job_id_list= new PrintWriter(new FileWriter("job_arrival_id.txt", true));
        Random rn = new Random();

        newclass n;
        n = new newclass();
        int type = 7;
        for (int j = 0; j < 480; j++) {
          
            int GPU_number = Integer.parseInt(br_job_arrival_id.readLine());
            int tot_ep = Integer.parseInt(br_totep.readLine());
            Job job = new Job(writer, Count++, tot_ep, GPU_number);
            synchronized (Job_list1) {
                Job_list1.add(job);
                Job_list_common.add(job);
            }
          
            Thread.sleep(Integer.parseInt(br_arrival.readLine()) * 10);
        }

        float time1 = 0, time2 = 0;
        System.out.println("Number of Jobs: " + Job_list_common.size() + " Total number of GPU " + total_gpu);

        for (int i = 0; i < Job_list_common.size(); i++) {

            time1 += (Job_list_common.get(i).get_finished_time()); //- (Job_list_common.get(i).get_start_time()*4.0)))+1;

        }
        time1 /= (Job_list_common.size() * 1.0);
        System.out.println("Average JCT: " + time1);
        br_arrival.close();
        br_totep.close();

    }

    static void tiresias_scheduler() throws InterruptedException, FileNotFoundException, IOException {
        BufferedReader br_arrival = new BufferedReader(new FileReader(file_arrival));
        BufferedReader br_totep = new BufferedReader(new FileReader(file_totep));
        BufferedReader br_job_arrival_id = new BufferedReader(new FileReader(file_JobArrival_ID));
        //List<Integer> possible_gpu = gpu_descriptor();

       // List<Integer> possible_job = job_descriptor();
        int Count = 0;

        //for(int p=10 ; p<=480 ; p+=10){
        File f = new File("normal.txt");
        PrintWriter job_id_list= new PrintWriter(new FileWriter("job_arrival_id.txt", true));
        Random rn = new Random();

        newclass2 n;
        n = new newclass2();
        int type = 7;
        for (int j = 0; j < 480; j++) {
          /*  int x = Math.abs(rn.nextInt() % type);

            if (possible_job.get(x) <= 0) {
                possible_job.remove(possible_job.get(x));
                possible_gpu.remove(x);
                type--;
                j--;
                continue;
            }*/
           // job_id_list.println(possible_gpu.get(x));
            int GPU_number = Integer.parseInt(br_job_arrival_id.readLine());
            int tot_ep = Integer.parseInt(br_totep.readLine());
         
            
            Job job = new Job(writer, Count++, tot_ep, GPU_number);
            synchronized (Job_list1) {
                Job_list1.add(job);
                Job_list_common.add(job);
            }
           /* int p = possible_job.get(x);
            possible_job.remove(x);
            p--;
            possible_job.add(x, p);*/
         //   System.out.println(GPU_number + " " + possible_job.get(x));
            Thread.sleep(Integer.parseInt(br_arrival.readLine()) * 10); 
        }

        float time1 = 0, time2 = 0;
        System.out.println("Number of Jobs: " + Job_list_common.size() + " Total number of GPU " + total_gpu);

        for (int i = 0; i < Job_list_common.size(); i++) {

            time1 += (Job_list_common.get(i).get_finished_time()); //- (Job_list_common.get(i).get_start_time()*4.0)))+1;

        }
        job_id_list.close();
    }

    static List gpu_descriptor() {

        List<Integer> possible_gpu = new ArrayList<>();
        possible_gpu.add(1);
        possible_gpu.add(2);
        possible_gpu.add(3);
        possible_gpu.add(4);
        possible_gpu.add(8);
        possible_gpu.add(16);
        possible_gpu.add(32);

        return possible_gpu;
    }

    static List job_descriptor() {
        List<Integer> possible_job = new ArrayList<>();
        possible_job.add(240);
        possible_job.add(40);
        possible_job.add(20);
        possible_job.add(60);
        possible_job.add(90);
        possible_job.add(25);
        possible_job.add(5);
        return possible_job;
    }

    static ArrayList Job_Description() throws IOException {
        File f = new File("Job.txt");
        ArrayList<String> job_desc = new ArrayList();
        BufferedReader br = new BufferedReader(new FileReader(f));
        String Job;
        while ((Job = br.readLine()) != null) {
            //System.out.println(Job);
            job_desc.add(Job);
            String st[] = Job.split(" ");
            int x = Integer.parseInt(st[0]);
            int y = Integer.parseInt(st[1]);
            // System.out.println(x+" "+y);
        }
        return job_desc;

    }

    public static class newclass implements Runnable {

        E_LAS_utilization scheduler;
        Thread th;

        public newclass() throws InterruptedException {

            th = new Thread(this);
            System.out.println("th before");
            th.start();
            System.out.println("th after");
            // th.join();
        }

        @Override
        public void run() {
            try {
                scheduler = new E_LAS_utilization(Job_list1, total_gpu, writer, 1);
            } catch (IOException ex) {
                Logger.getLogger(GpuUtilization.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

    public static class newclass2 implements Runnable {

        tiresias scheduler;
        Thread th;

        public newclass2() throws InterruptedException {

            th = new Thread(this);
            System.out.println("th before");
            th.start();
            System.out.println("th after");
            // th.join();
        }

        @Override
        public void run() {
            try {
                scheduler = new tiresias(Job_list1, total_gpu, writer, 1);
            } catch (IOException ex) {
                Logger.getLogger(GpuUtilization.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }
}
