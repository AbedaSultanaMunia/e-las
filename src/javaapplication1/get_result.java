/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication1;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

/**
 *
 * @author abeda
 */
public class get_result {
// Size diFileNameibution Normal

    static File[] file = new File[12];
    static String[] FileName = new String[12];

    // Size diFileNameibution Exponential
    /* 
    FileName[0] = "Normal/norm/log_norm_norm.txt";
        FileName[1] = "Normal/norm/log_tri_norm_norm.txt";
        FileName[2] = "Normal/norm/log_YARN_norm_norm.txt";
        FileName[3] = "Normal/norm/log_SRTF_norm_norm.txt";

        FileName[4] = "Normal/expo/log_norm_expo.txt";
        FileName[5] = "Normal/expo/log_tri_norm_expo.txt";
        FileName[6] = "Normal/expo/log_YARN_norm_expo.txt";
        FileName[7] = "Normal/expo/log_SRTF_norm_expo.txt";

        FileName[8] = "Normal/pois/log_norm_pois.txt";
        FileName[9] = "Normal/pois/log_tri_norm_pois.txt";
        FileName[10] = "Normal/pois/log_YARN_norm_pois.txt";
        FileName[11] = "Normal/pois/log_SRTF_norm_pois.txt";
    
    File file_Normal_elas = new File("Exponential/norm/log_expo_norm.txt");
    File file_Normal_tri = new File("Exponential/norm/log_tri_expo_norm.txt");
    File file_Normal_srtf = new File("Exponential/norm/log_YARN_expo_norm.txt");
    File file_Normal_yarn = new File("Exponential/norm/log_SRTF_expo_norm.txt");
    
    File file_expo_elas = new File("Exponential/expo/log_expo_expo.txt");
    File file_expo_tri = new File("Exponential/expo/log_tri_expo_expo.txt");
    File file_expo_srtf = new File("Exponential/expo/log_YARN_expo_expo.txt");
    File file_expo_yarn = new File("Exponential/expo/log_SRTF_expo_expo.txt");
    
    File file_pois_elas = new File("Exponential/pois/log_expo_pois.txt");
    File file_pois_tri = new File("Exponential/pois/log_tri_expo_pois.txt");
    File file_pois_srtf = new File("Exponential/pois/log_YARN_expo_pois.txt");
    File file_pois_yarn = new File("Exponential/pois/log_SRTF_expo_pois.txt");*/
    // Size diFileNameibution Poisson
    /*  File file_Normal_elas = new File("Poisson/norm/log_pois_norm.txt");
    File file_Normal_tri = new File("Poisson/norm/log_tri_pois_norm.txt");
    File file_Normal_srtf = new File("Poisson/norm/log_YARN_pois_norm.txt");
    File file_Normal_yarn = new File("Poisson/norm/log_SRTF_pois_norm.txt");
    
    File file_expo_elas = new File("Poisson/expo/log_pois_expo.txt");
    File file_expo_tri = new File("Poisson/expo/log_tri_pois_expo.txt");
    File file_expo_srtf = new File("Poisson/expo/log_YARN_pois_expo.txt");
    File file_expo_yarn = new File("Poisson/expo/log_SRTF_pois_expo.txt");
    
    File file_pois_elas = new File("Poisson/pois/log_pois_expo.txt");
    File file_pois_tri = new File("Poisson/pois/log_tri_pois_expo.txt");
    File file_pois_srtf = new File("Poisson/pois/log_YARN_pois_expo.txt");
    File file_pois_yarn = new File("Poisson/pois/log_SRTF_pois_expo.txt");*/
    public static void main(String[] args) throws FileNotFoundException, IOException {
       /* FileName[0] = "Poisson/norm/log_pois_norm.txt";
        FileName[1] = "Poisson/norm/log_tri_pois_norm.txt";
        FileName[2] = "Poisson/norm/log_YARN_pois_norm.txt";
        FileName[3] = "Poisson/norm/log_SRTF_pois_norm.txt";

        FileName[4] = "Poisson/expo/log_pois_expo.txt";
        FileName[5] = "Poisson/expo/log_tri_pois_expo.txt";
        FileName[6] = "Poisson/expo/log_YARN_pois_expo.txt";
        FileName[7] = "Poisson/expo/log_SRTF_pois_expo.txt";*/

        FileName[8] = "Poisson/pois/log_pois_expo.txt";
        FileName[9] = "Poisson/pois/log_tri_pois_expo.txt";
        FileName[10] = "Poisson/pois/log_YARN_pois_expo.txt";
        FileName[11] = "Poisson/pois/log_SRTF_pois_expo.txt";

        String _str;

        for (int i = 8; i < 12; i++) {
            file[i] = new File(FileName[i]);
            BufferedReader br = new BufferedReader(new FileReader(file[i]));
             PrintWriter writer = new PrintWriter(new FileWriter(FileName[i]+"output.txt", true));
             int count =0;
            while ((_str = br.readLine()) != null) {
                count++;
                _str = _str.trim();
                String splitted_line[] = _str.split(" ");
                
                //System.out.println(splitted_line.length+"*** "+_str);
                if (splitted_line.length < 11 || splitted_line.length == 1) {
                    continue;
                }  
                String split2[] = splitted_line[0].split("_");
                writer.println(split2[0]+"----------------------"+splitted_line[6]);
               writer.flush();
            }
        
            writer.close();
            
        }
    }
}
