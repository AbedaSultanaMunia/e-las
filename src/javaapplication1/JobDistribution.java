/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication1;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Random;
import java.lang.Object;
import org.apache.commons.math3.distribution.*;
import org.apache.commons.math3.random.*;

/**
 *
 * @author abeda
 */
public class JobDistribution {

    static Random rand = new Random();

    public static void main(String[] args) throws IOException, InterruptedException {
        //poisson_arrival_rate();
        // PrintWriter normal_dist_writer = new PrintWriter(new FileWriter("normal.txt", true));

        // PrintWriter poisson_dist_writer = new PrintWriter(new FileWriter("poisson.txt", true));
        // PrintWriter epoch_norm = new PrintWriter(new FileWriter("ep_norm.txt", true)) ;
        //PrintWriter epoch_expo = new PrintWriter(new FileWriter("ep_expo.txt", true));
        //PrintWriter epoch_poiss = new PrintWriter(new FileWriter("ep_poiss.txt", true));
       // PrintWriter expo_dist_writer = new PrintWriter(new FileWriter("norm.txt", true));
        PoissonDistribution pd = new PoissonDistribution(30, 4);
        NormalDistribution nd = new NormalDistribution(30, 4);
        ExponentialDistribution ed = new ExponentialDistribution(30);
        File f = new File("normal.txt");
        BufferedReader br = new BufferedReader(new FileReader(f));
        String tot_epoch;

        for (int i = 0; i < 480; i++) {

            tot_epoch = br.readLine();
            int tot_epoch_int = Integer.parseInt(tot_epoch);
            System.out.println(tot_epoch_int);
            String _file =i + "ep_norm.txt";
           
            PrintWriter epoch_norm = new PrintWriter(new FileWriter(_file, true));
            while (tot_epoch_int > 0) {
                int x = (int) nd.sample();

                tot_epoch_int -= x;

                // System.out.println(tot_epoch_int);
                if (tot_epoch_int >= 0) {
                    epoch_norm.println(x);
                } else {
                    tot_epoch_int += x;
                    break;
                }
            }

            if (tot_epoch_int != 0) {
                epoch_norm.println(tot_epoch_int);
            }
            epoch_norm.close();
        }
        //break;
        //normal_dist_writer.println(getNormalRandom());
        // poisson_dist_writer.println(pd.sample());
        // expo_dist_writer.println(getExpoDist());
        //   normal_dist_writer.close();
        // poisson_dist_writer.close();
        //expo_dist_writer.close();
    }

    static void poisson_arrival_rate() {
        PoissonDistribution pd = new PoissonDistribution(10, 4);
        int i = 0, p = 0;
        while (i < 480) {
            int x = (int) pd.sample();
            System.out.println(x + p);
            p += x;
            i++;
        }
    }

    static int getPoissonRandom() {
        double mean = 1000;
        Random r = new Random();
        double L = Math.exp(-mean);
        int k = 0;
        double p = 1000.0;
        do {
            p = p * r.nextDouble();
            k++;
        } while (p > L);
        return k - 1;
    }

    static int getNormalRandom() {
        int epoch_i;
        do {
            double val = rand.nextGaussian() * 1000 + 1000;

            epoch_i = (int) Math.round(val);

        } while (epoch_i <= 0);
        return epoch_i;
    }

    static int getExpoDist() {
        Random r = rand;

        double p = 1 / 1000.0;
        int y = (int) -(Math.log(r.nextDouble()) / p);
        return y;
    }

    static int getConstantDist(Job j) {
        return 10;
    }

    static int getincreasingDist(Job j, int epoch_i) {
        return epoch_i + 50;
    }

    static int getdecreasingDist(Job j, int epoch_i) {
        return epoch_i - 50;
    }

}
