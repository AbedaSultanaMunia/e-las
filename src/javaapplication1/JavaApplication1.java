/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication1;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author abeda
 */
public class JavaApplication1 {

    /**
     * @param args the command line arguments
     */
    static int total_gpu = 2;

    public static void main(String[] args) throws IOException, InterruptedException {

     
        int distribution = 1;

        PrintWriter writer = new PrintWriter(new FileWriter("log_pois_expo.txt"));
        PrintWriter writer2 = new PrintWriter(new FileWriter("log_tri_pois_expo.txt"));
        PrintWriter writer4 = new PrintWriter(new FileWriter("log_YARN_pois_expo.txt"));
        PrintWriter writer5 = new PrintWriter(new FileWriter("log_SRTF_pois_n.txt"));

       // PrintWriter writer3 = new PrintWriter(new FileWriter("JCT.txt", true));

        ArrayList<String> Job_Desc = Job_Description();
        int list_sz = Job_Desc.size();
        String st[];
        int Count = 0;

        //for(int p=10 ; p<=480 ; p+=10){
        List<Job> Job_list1 = new ArrayList<>();
        List<Job> Job_list_common = new ArrayList<>();
        List<Job> Job_list2 = new ArrayList<>();
        List<Job> Job_list3 = new ArrayList<>();
        List<Job> Job_list4 = new ArrayList<>();
        File f = new File("normal.txt");
        BufferedReader br = new BufferedReader(new FileReader(f));

        for (int i = 0; i < list_sz; i++) {
            st = Job_Desc.get(i).split(" ");
            int Job_number = Integer.parseInt(st[0]);
            int GPU_number = Integer.parseInt(st[1]);
            //  int toteps = Integer.parseInt(st[2]);
            //  int eprate = Integer.parseInt(st[3]);
            for (int j = 0; j < Job_number; j++) {
                // Job job = new Job(writer, Count++, GPU_number, toteps, eprate);
                int tot_ep = Integer.parseInt(br.readLine());
                Job job = new Job(writer, Count++, tot_ep, GPU_number);
                Job_list1.add(job);
                Job_list_common.add(job);
                Job_list2.add(job);
                Job_list3.add(job);
                Job_list4.add(job);
                Thread.sleep(getPoissonRandom());
            }
        }

        Scheduler scheduler = new Scheduler(Job_list1, total_gpu, writer, distribution);

        float time1 = 0, time2 = 0;
        System.out.println("Number of Jobs: " + Job_list_common.size() + " Total number of GPU " + total_gpu);

        for (int i = 0; i < Job_list_common.size(); i++) {

            time1 += (Job_list_common.get(i).get_finished_time()); //- (Job_list_common.get(i).get_start_time()*4.0)))+1;

        }
        time1 /= (Job_list_common.size() * 1.0);
        System.out.println("Average JCT: " + time1);

        int[] result_elas = CalcResult(Job_list_common);

        //***********************************************************************************
        for (int i = 0; i < Job_list2.size(); i++) {
            Job j = Job_list2.get(i);
            j.reinitialization();
        }
        tiresias tri_scheduler = new tiresias(Job_list2, total_gpu, writer2, distribution);

        time2 = 0;
        for (int i = 0; i < Job_list_common.size(); i++) {
            //System.out.println(Job_list_common.get(i).get_ID()+"  "+Job_list_common.get(i).get_finished_time());
            time2 += (Job_list_common.get(i).get_finished_time());//- (Job_list_common.get(i).get_start_time()*4.0)))+1;
        }
        time2 /= (Job_list_common.size() * 1.0);
        System.out.println("Average JCT: for tiresias " + time2);

        int[] result_las = CalcResult(Job_list_common);
        //*********************************************************************************************

        for (int i = 0; i < Job_list4.size(); i++) {
            Job j = Job_list4.get(i);
            j.reinitialization2();
        }
        SRTF SRTF_scheduler = new SRTF(Job_list4, total_gpu, writer5, distribution);

        float time4 = 0;
        for (int i = 0; i < Job_list_common.size(); i++) {
            // System.out.println(Job_list_common.get(i).get_ID()+"  "+Job_list_common.get(i).get_finished_time());
            time4 += (Job_list_common.get(i).get_finished_time());//- (Job_list_common.get(i).get_start_time()*4.0)))+1;
        }
        time4 /= (Job_list_common.size() * 1.0);
        System.out.println("Average JCT: for SRTF " + time4);

        int[] result_srtf = CalcResult(Job_list_common);
        /**
         * ******************************************************************************************************************
         */
        for (int i = 0; i < Job_list3.size(); i++) {
            Job j = Job_list3.get(i);
            j.reinitialization2();
        }

        YARN yarn_scheduler = new YARN(Job_list3, total_gpu, writer4, distribution);

        float time3 = 0;

        for (int i = 0; i < Job_list_common.size(); i++) {
            time3 += (Job_list_common.get(i).get_finished_time());//- (Job_list_common.get(i).get_start_time()*4.0)))+1;
        }
        time3 /= (Job_list_common.size() * 1.0);
        System.out.println("Average JCT: for YARN " + time3);

        int[] result_YARN = CalcResult(Job_list_common);
        //*********************************************************************************************

        //    writer3.println(Job_list_common.size() + " " + time1 + " " + time2 + " " + time3+" "+time4);
        int queuingdelay_LAS = 0;
        int queuingdelay_ELAS = 0;
        int queuingdelay_YARN = 0;
        int queuingdelay_SRTF = 0;
        int queue_delay_ELAS[] = new int[Job_list_common.size()];
        int queue_delay_LAS[] = new int[Job_list_common.size()];
        int queue_delay_YARN[] = new int[Job_list_common.size()];
        int queue_delay_SRTF[] = new int[Job_list_common.size()];

        for (int i = 0; i < Job_list_common.size(); i++) {
            // System.out.println(Job_list_common.get(i).timelas);
            queuingdelay_ELAS += Job_list_common.get(i).queuing_delay_elas;
            queue_delay_ELAS[i] = Job_list_common.get(i).queuing_delay_elas;
            //   System.out.println(Job_list_common.get(i).timeelas);
            queuingdelay_LAS += Job_list_common.get(i).queuing_delay_las;
            queue_delay_LAS[i] = Job_list_common.get(i).queuing_delay_las;
            //  System.out.println(Job_list_common.get(i).timeYARN+"\n\n");
            queuingdelay_YARN += Job_list_common.get(i).queuing_delay_YARN;
            queue_delay_YARN[i] = Job_list_common.get(i).queuing_delay_YARN;

            queuingdelay_SRTF += Job_list_common.get(i).queuing_delay_SRTF;
            queue_delay_SRTF[i] = Job_list_common.get(i).queuing_delay_SRTF;
        }
        mean_median_95th("ELAS", queue_delay_ELAS);
        mean_median_95th("LAS", queue_delay_LAS);
        mean_median_95th("YARN", queue_delay_YARN);
        mean_median_95th("SRTF", queue_delay_SRTF);

        System.out.println("************************************************************");
        // }
        System.out.println("Queuing delay LAS: " + queuingdelay_LAS + "------ELAS: "
                + queuingdelay_ELAS + "---------YARN: " + queuingdelay_YARN + "---------SRTF: " + queuingdelay_SRTF);

        for (int i = 1; i <= 4; i++) {
            System.out.println(i + " -> LAS: " + result_las[i] + " E-LAS: " + result_elas[i] + " YARN: " + result_YARN[i] + " SRTF: " + result_srtf[i]);

        }
        writer.close();
        writer2.close();
        //writer3.close();

    }

    static void mean_median_95th(String algo, int[] a) {
        int sum = 0;
        int n = a.length;
        for (int i = 0; i < n; i++) {
            sum += a[i];
        }
        double mean = (double) sum / (double) n;
        System.out.println("Mean QD for " + algo + " : " + mean);

        Arrays.sort(a);
        Double median;
        // check for even case 
        if (n % 2 != 0) {
            median = (double) a[n / 2];
        }

        median = (double) (a[(n - 1) / 2] + a[n / 2]) / 2.0;

        System.out.println("Median QD for " + algo + " : " + median);
        
        double percentile_95th = sum * 0.95;
        sum =0;
         for (int i = 0; i < n; i++) {
             sum+= a[i];
             if(sum >= percentile_95th)
             {
                   System.out.println("Percentile 95th for  " + algo + " : " + a[i]);
                   break;
             }
         }

    }

    static ArrayList Job_Description() throws IOException {
        File f = new File("Job.txt");
        ArrayList<String> job_desc = new ArrayList();
        BufferedReader br = new BufferedReader(new FileReader(f));
        String Job;
        while ((Job = br.readLine()) != null) {
            //System.out.println(Job);
            job_desc.add(Job);
            String st[] = Job.split(" ");
            int x = Integer.parseInt(st[0]);
            int y = Integer.parseInt(st[1]);
            // System.out.println(x+" "+y);
        }
        return job_desc;

    }

    static ArrayList Job_Description2() throws IOException {
        File f = new File("Job2.txt");
        ArrayList<String> job_desc = new ArrayList();
        BufferedReader br = new BufferedReader(new FileReader(f));
        String Job;
        while ((Job = br.readLine()) != null) {
            job_desc.add(Job);

        }
        return job_desc;

    }

    static int getPoissonRandom() {
        double mean = 30;
        Random r = new Random();
        double L = Math.exp(-mean);
        int k = 0;
        double p = 1.0;
        do {
            p = p * r.nextDouble();
            k++;
        } while (p > L);
        return k - 1;
    }

    static int[] CalcResult(List<Job> Job_list) {
        int[] Gpu_job = new int[5];
        int[] time = new int[5];
        for (int i = 0; i < Job_list.size(); i++) {
            if (Job_list.get(i).get_gpu() < 4) {
                time[Job_list.get(i).get_gpu()] += Job_list.get(i).get_finished_time();
                Gpu_job[Job_list.get(i).get_gpu()]++;
            } else {
                time[4] += Job_list.get(i).get_finished_time();
                Gpu_job[4]++;
            }

        }
        for(int i=0; i< 5; i++)
        {
            if(Gpu_job[i]!=0)
            System.out.println("Gpu job "+Gpu_job[i]+" ---- avg Jct: "+time[i]/Gpu_job[i]);
        }

        return time;
    }

}
